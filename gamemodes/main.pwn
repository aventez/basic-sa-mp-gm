/*  Base of SA:MP GameMode
*   -
*   Author: 	  Filip (Discord: Filip#0996)
*   Published: 	  21.12.2018
*   Version:      1.0
*	-
*	If you delete this footer, add author to credits. Respect other work's.
*/

//Loading include's
#include <a_samp>
#include <a_mysql>

#include <streamer>
#include <sscanf2>
#include <md5>
#include <zcmd>

//Macro's
new sprintfstr[1024];
#define sprintf(%0,%1) (format(sprintfstr, 1024, %0, %1), sprintfstr) //Shorter format function, possible to use in argument
#pragma unused sprintfstr

#define Timer_Start(%0) new timer_%0 = GetTickCount()
#define Timer_End(%0) (GetTickCount() - timer_%0) 		//For use in functions

//Loading module's
#include "modules/config.inc"			//All configuration and definiotons etc.
#include "modules/basic_functions.inc"	//Basic script functions, mainly relating to gamemode 
#include "modules/player_functions.inc" //All player functions, mostly main player's publics

main() {}